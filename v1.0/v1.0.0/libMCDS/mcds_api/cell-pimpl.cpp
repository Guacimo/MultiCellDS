// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

// Begin prologue.
//
//
// End prologue.

#include "cell-pimpl.hpp"

#include <xsde/cxx/parser/validating/string-common.hxx>

namespace cell
{
  // population_definition_pimpl
  //

  population_definition_pimpl::
  population_definition_pimpl (bool b)
  {
    this->population_definition_pimpl_base_ = b;
    this->population_definition_pimpl_state_.population_definition_ = 0;
  }

  population_definition_pimpl::
  ~population_definition_pimpl ()
  {
    if (!this->population_definition_pimpl_base_ && this->population_definition_pimpl_state_.population_definition_)
      delete this->population_definition_pimpl_state_.population_definition_;
  }

  void population_definition_pimpl::
  _reset ()
  {
    population_definition_pskel::_reset ();

    if (!this->population_definition_pimpl_base_ && this->population_definition_pimpl_state_.population_definition_)
    {
      delete this->population_definition_pimpl_state_.population_definition_;
      this->population_definition_pimpl_state_.population_definition_ = 0;
    }
  }

  void population_definition_pimpl::
  pre_impl (::cell::population_definition* x)
  {
    this->population_definition_pimpl_state_.population_definition_ = x;
  }

  void population_definition_pimpl::
  pre ()
  {
    ::cell::population_definition* x = new ::cell::population_definition;
    this->pre_impl (x);
  }

  void population_definition_pimpl::
  ID (unsigned int x)
  {
    this->population_definition_pimpl_state_.population_definition_->ID (x);
  }

  void population_definition_pimpl::
  name (const ::std::string& x)
  {
    this->population_definition_pimpl_state_.population_definition_->name (x);
  }

  void population_definition_pimpl::
  units (const ::std::string& x)
  {
    this->population_definition_pimpl_state_.population_definition_->units (x);
  }

  void population_definition_pimpl::
  phenotype_dataset (::phenotype_dataset::phenotype_dataset* x)
  {
    this->population_definition_pimpl_state_.population_definition_->phenotype_dataset (x);
  }

  void population_definition_pimpl::
  custom (::common::custom* x)
  {
    this->population_definition_pimpl_state_.population_definition_->custom (x);
  }

  ::cell::population_definition* population_definition_pimpl::
  post_population_definition ()
  {
    ::cell::population_definition* r = this->population_definition_pimpl_state_.population_definition_;
    this->population_definition_pimpl_state_.population_definition_ = 0;
    return r;
  }

  // population_definitions_pimpl
  //

  population_definitions_pimpl::
  population_definitions_pimpl (bool b)
  {
    this->population_definitions_pimpl_base_ = b;
    this->population_definitions_pimpl_state_.population_definitions_ = 0;
  }

  population_definitions_pimpl::
  ~population_definitions_pimpl ()
  {
    if (!this->population_definitions_pimpl_base_ && this->population_definitions_pimpl_state_.population_definitions_)
      delete this->population_definitions_pimpl_state_.population_definitions_;
  }

  void population_definitions_pimpl::
  _reset ()
  {
    population_definitions_pskel::_reset ();

    if (!this->population_definitions_pimpl_base_ && this->population_definitions_pimpl_state_.population_definitions_)
    {
      delete this->population_definitions_pimpl_state_.population_definitions_;
      this->population_definitions_pimpl_state_.population_definitions_ = 0;
    }
  }

  void population_definitions_pimpl::
  pre_impl (::cell::population_definitions* x)
  {
    this->population_definitions_pimpl_state_.population_definitions_ = x;
  }

  void population_definitions_pimpl::
  pre ()
  {
    ::cell::population_definitions* x = new ::cell::population_definitions;
    this->pre_impl (x);
  }

  void population_definitions_pimpl::
  population_definition (::cell::population_definition* x)
  {
    this->population_definitions_pimpl_state_.population_definitions_->population_definition ().push_back (x);
  }

  void population_definitions_pimpl::
  custom (::common::custom* x)
  {
    this->population_definitions_pimpl_state_.population_definitions_->custom (x);
  }

  ::cell::population_definitions* population_definitions_pimpl::
  post_population_definitions ()
  {
    ::cell::population_definitions* r = this->population_definitions_pimpl_state_.population_definitions_;
    this->population_definitions_pimpl_state_.population_definitions_ = 0;
    return r;
  }

  // cell_pimpl
  //

  cell_pimpl::
  cell_pimpl (bool b)
  {
    this->cell_pimpl_base_ = b;
    this->cell_pimpl_state_.cell_ = 0;
  }

  cell_pimpl::
  ~cell_pimpl ()
  {
    if (!this->cell_pimpl_base_ && this->cell_pimpl_state_.cell_)
      delete this->cell_pimpl_state_.cell_;
  }

  void cell_pimpl::
  _reset ()
  {
    cell_pskel::_reset ();

    if (!this->cell_pimpl_base_ && this->cell_pimpl_state_.cell_)
    {
      delete this->cell_pimpl_state_.cell_;
      this->cell_pimpl_state_.cell_ = 0;
    }
  }

  void cell_pimpl::
  pre_impl (::cell::cell* x)
  {
    this->cell_pimpl_state_.cell_ = x;
  }

  void cell_pimpl::
  pre ()
  {
    ::cell::cell* x = new ::cell::cell;
    this->pre_impl (x);
  }

  void cell_pimpl::
  ID (unsigned int x)
  {
    this->cell_pimpl_state_.cell_->ID (x);
  }

  void cell_pimpl::
  phenotype_dataset (::phenotype_dataset::phenotype_dataset* x)
  {
    this->cell_pimpl_state_.cell_->phenotype_dataset (x);
  }

  void cell_pimpl::
  state (::state::state* x)
  {
    this->cell_pimpl_state_.cell_->state (x);
  }

  void cell_pimpl::
  custom (::common::custom* x)
  {
    this->cell_pimpl_state_.cell_->custom (x);
  }

  ::cell::cell* cell_pimpl::
  post_cell ()
  {
    ::cell::cell* r = this->cell_pimpl_state_.cell_;
    this->cell_pimpl_state_.cell_ = 0;
    return r;
  }

  // cell_population_individual_pimpl
  //

  cell_population_individual_pimpl::
  cell_population_individual_pimpl (bool b)
  {
    this->cell_population_individual_pimpl_base_ = b;
    this->cell_population_individual_pimpl_state_.cell_population_individual_ = 0;
  }

  cell_population_individual_pimpl::
  ~cell_population_individual_pimpl ()
  {
    if (!this->cell_population_individual_pimpl_base_ && this->cell_population_individual_pimpl_state_.cell_population_individual_)
      delete this->cell_population_individual_pimpl_state_.cell_population_individual_;
  }

  void cell_population_individual_pimpl::
  _reset ()
  {
    cell_population_individual_pskel::_reset ();

    if (!this->cell_population_individual_pimpl_base_ && this->cell_population_individual_pimpl_state_.cell_population_individual_)
    {
      delete this->cell_population_individual_pimpl_state_.cell_population_individual_;
      this->cell_population_individual_pimpl_state_.cell_population_individual_ = 0;
    }
  }

  void cell_population_individual_pimpl::
  pre_impl (::cell::cell_population_individual* x)
  {
    this->cell_population_individual_pimpl_state_.cell_population_individual_ = x;
  }

  void cell_population_individual_pimpl::
  pre ()
  {
    ::cell::cell_population_individual* x = new ::cell::cell_population_individual;
    this->pre_impl (x);
  }

  void cell_population_individual_pimpl::
  type (const ::std::string& x)
  {
    this->cell_population_individual_pimpl_state_.cell_population_individual_->type (x);
  }

  void cell_population_individual_pimpl::
  population_ID (unsigned int x)
  {
    this->cell_population_individual_pimpl_state_.cell_population_individual_->population_ID (x);
  }

  void cell_population_individual_pimpl::
  cell (::cell::cell* x)
  {
    this->cell_population_individual_pimpl_state_.cell_population_individual_->cell ().push_back (x);
  }

  void cell_population_individual_pimpl::
  custom (::common::custom* x)
  {
    this->cell_population_individual_pimpl_state_.cell_population_individual_->custom (x);
  }

  ::cell::cell_population_individual* cell_population_individual_pimpl::
  post_cell_population_individual ()
  {
    ::cell::cell_population_individual* r = this->cell_population_individual_pimpl_state_.cell_population_individual_;
    this->cell_population_individual_pimpl_state_.cell_population_individual_ = 0;
    return r;
  }

  // cell_population_aggregate_pimpl
  //

  cell_population_aggregate_pimpl::
  cell_population_aggregate_pimpl (bool b)
  {
    this->cell_population_aggregate_pimpl_base_ = b;
    this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_ = 0;
  }

  cell_population_aggregate_pimpl::
  ~cell_population_aggregate_pimpl ()
  {
    if (!this->cell_population_aggregate_pimpl_base_ && this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_)
      delete this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_;
  }

  void cell_population_aggregate_pimpl::
  _reset ()
  {
    cell_population_aggregate_pskel::_reset ();

    if (!this->cell_population_aggregate_pimpl_base_ && this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_)
    {
      delete this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_;
      this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_ = 0;
    }
  }

  void cell_population_aggregate_pimpl::
  pre_impl (::cell::cell_population_aggregate* x)
  {
    this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_ = x;
  }

  void cell_population_aggregate_pimpl::
  pre ()
  {
    ::cell::cell_population_aggregate* x = new ::cell::cell_population_aggregate;
    this->pre_impl (x);
  }

  void cell_population_aggregate_pimpl::
  type (const ::std::string& x)
  {
    this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_->type (x);
  }

  void cell_population_aggregate_pimpl::
  population_ID (unsigned int x)
  {
    this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_->population_ID (x);
  }

  void cell_population_aggregate_pimpl::
  value (::common::units_decimal* x)
  {
    this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_->value (x);
  }

  void cell_population_aggregate_pimpl::
  sequence_present ()
  {
    ::cell::cell_population_aggregate::sequence_type* x = new ::cell::cell_population_aggregate::sequence_type;
    this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_->sequence (x);
  }

  void cell_population_aggregate_pimpl::
  phenotype_dataset (::phenotype_dataset::phenotype_dataset* x)
  {
    this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_->sequence ().phenotype_dataset (x);
  }

  void cell_population_aggregate_pimpl::
  state (::state::state* x)
  {
    this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_->sequence ().state (x);
  }

  void cell_population_aggregate_pimpl::
  custom (::common::custom* x)
  {
    this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_->sequence ().custom (x);
  }

  ::cell::cell_population_aggregate* cell_population_aggregate_pimpl::
  post_cell_population_aggregate ()
  {
    ::cell::cell_population_aggregate* r = this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_;
    this->cell_population_aggregate_pimpl_state_.cell_population_aggregate_ = 0;
    return r;
  }

  // population_vector_pimpl
  //

  population_vector_pimpl::
  population_vector_pimpl (bool b)
  {
    this->population_vector_pimpl_base_ = b;
    this->population_vector_pimpl_state_.population_vector_ = 0;
  }

  population_vector_pimpl::
  ~population_vector_pimpl ()
  {
    if (!this->population_vector_pimpl_base_ && this->population_vector_pimpl_state_.population_vector_)
      delete this->population_vector_pimpl_state_.population_vector_;
  }

  void population_vector_pimpl::
  _reset ()
  {
    population_vector_pskel::_reset ();

    if (!this->population_vector_pimpl_base_ && this->population_vector_pimpl_state_.population_vector_)
    {
      delete this->population_vector_pimpl_state_.population_vector_;
      this->population_vector_pimpl_state_.population_vector_ = 0;
    }
  }

  void population_vector_pimpl::
  pre_impl (::cell::population_vector* x)
  {
    this->population_vector_pimpl_state_.population_vector_ = x;
  }

  void population_vector_pimpl::
  pre ()
  {
    ::cell::population_vector* x = new ::cell::population_vector;
    this->pre_impl (x);
  }

  void population_vector_pimpl::
  voxel_ID (::common::unsigned_int_list* x)
  {
    this->population_vector_pimpl_state_.population_vector_->voxel_ID (x);
  }

  void population_vector_pimpl::
  value (::common::units_double_list* x)
  {
    this->population_vector_pimpl_state_.population_vector_->value (x);
  }

  void population_vector_pimpl::
  cell_population (::cell::cell_population_aggregate* x)
  {
    this->population_vector_pimpl_state_.population_vector_->cell_population ().push_back (x);
  }

  void population_vector_pimpl::
  custom (::common::custom* x)
  {
    this->population_vector_pimpl_state_.population_vector_->custom (x);
  }

  ::cell::population_vector* population_vector_pimpl::
  post_population_vector ()
  {
    ::cell::population_vector* r = this->population_vector_pimpl_state_.population_vector_;
    this->population_vector_pimpl_state_.population_vector_ = 0;
    return r;
  }

  // cell_populations_pimpl
  //

  cell_populations_pimpl::
  cell_populations_pimpl (bool b)
  {
    this->cell_populations_pimpl_base_ = b;
    this->cell_populations_pimpl_state_.cell_populations_ = 0;
  }

  cell_populations_pimpl::
  ~cell_populations_pimpl ()
  {
    if (!this->cell_populations_pimpl_base_ && this->cell_populations_pimpl_state_.cell_populations_)
      delete this->cell_populations_pimpl_state_.cell_populations_;
  }

  void cell_populations_pimpl::
  _reset ()
  {
    cell_populations_pskel::_reset ();

    if (!this->cell_populations_pimpl_base_ && this->cell_populations_pimpl_state_.cell_populations_)
    {
      delete this->cell_populations_pimpl_state_.cell_populations_;
      this->cell_populations_pimpl_state_.cell_populations_ = 0;
    }
  }

  void cell_populations_pimpl::
  pre_impl (::cell::cell_populations* x)
  {
    this->cell_populations_pimpl_state_.cell_populations_ = x;
  }

  void cell_populations_pimpl::
  pre ()
  {
    ::cell::cell_populations* x = new ::cell::cell_populations;
    this->pre_impl (x);
  }

  void cell_populations_pimpl::
  population_vector (::cell::population_vector* x)
  {
    this->cell_populations_pimpl_state_.cell_populations_->population_vector ().push_back (x);
  }

  void cell_populations_pimpl::
  cell_population (::cell::cell_population_individual* x)
  {
    this->cell_populations_pimpl_state_.cell_populations_->cell_population (x);
  }

  ::cell::cell_populations* cell_populations_pimpl::
  post_cell_populations ()
  {
    ::cell::cell_populations* r = this->cell_populations_pimpl_state_.cell_populations_;
    this->cell_populations_pimpl_state_.cell_populations_ = 0;
    return r;
  }

  // cellular_information_pimpl
  //

  cellular_information_pimpl::
  cellular_information_pimpl (bool b)
  {
    this->cellular_information_pimpl_base_ = b;
    this->cellular_information_pimpl_state_.cellular_information_ = 0;
  }

  cellular_information_pimpl::
  ~cellular_information_pimpl ()
  {
    if (!this->cellular_information_pimpl_base_ && this->cellular_information_pimpl_state_.cellular_information_)
      delete this->cellular_information_pimpl_state_.cellular_information_;
  }

  void cellular_information_pimpl::
  _reset ()
  {
    cellular_information_pskel::_reset ();

    if (!this->cellular_information_pimpl_base_ && this->cellular_information_pimpl_state_.cellular_information_)
    {
      delete this->cellular_information_pimpl_state_.cellular_information_;
      this->cellular_information_pimpl_state_.cellular_information_ = 0;
    }
  }

  void cellular_information_pimpl::
  pre_impl (::cell::cellular_information* x)
  {
    this->cellular_information_pimpl_state_.cellular_information_ = x;
  }

  void cellular_information_pimpl::
  pre ()
  {
    ::cell::cellular_information* x = new ::cell::cellular_information;
    this->pre_impl (x);
  }

  void cellular_information_pimpl::
  DCLs (::cell_line::DCLs* x)
  {
    this->cellular_information_pimpl_state_.cellular_information_->DCLs (x);
  }

  void cellular_information_pimpl::
  population_definitions (::cell::population_definitions* x)
  {
    this->cellular_information_pimpl_state_.cellular_information_->population_definitions (x);
  }

  void cellular_information_pimpl::
  mesh (::mesh::mesh* x)
  {
    this->cellular_information_pimpl_state_.cellular_information_->mesh (x);
  }

  void cellular_information_pimpl::
  cell_populations (::cell::cell_populations* x)
  {
    this->cellular_information_pimpl_state_.cellular_information_->cell_populations (x);
  }

  ::cell::cellular_information* cellular_information_pimpl::
  post_cellular_information ()
  {
    ::cell::cellular_information* r = this->cellular_information_pimpl_state_.cellular_information_;
    this->cellular_information_pimpl_state_.cellular_information_ = 0;
    return r;
  }
}

// Begin epilogue.
//
//
// End epilogue.

